import { defineConfig, loadEnv } from 'vite';
import react from '@vitejs/plugin-react-swc';
import path from 'path';

export default ({ mode }) => {
  process.env = { ...process.env, ...loadEnv(mode, process.cwd()) };

  // https://vitejs.dev/config/
  return defineConfig({
    plugins: [react()],
    server: {
      port: parseInt(process.env.VITE_PORT)
    },
    resolve: {
      alias: {
        '@': path.resolve(__dirname, './src/')
      }
    }
  });
};
